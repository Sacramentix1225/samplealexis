import hxd.Window;
import scene.Menu;

/**
 * Main class of the game launched when you start the program
 */

class Game extends hxd.App
{
    
    // Global variable of the game can be accesed  with Game.(insertVar) in any class
    /** current scene displayed on the screen **/
    public static var currentScene:DynamicScene;
    /** instance of the game used to change the current scene **/
    public static var instance:Game;
    

    static function main()
    {
        
        hxd.Res.initEmbed();
        instance = new Game();
        
    }
 

    /**
     * code below is executed 1 time when you start the app
     */

    override function init()
    {   
        
        currentScene = new scene.Menu();
        Window.getInstance().setFullScreen(true);
        this.setScene2D(currentScene);
        
     
    }

    /**
     * code below is executed each time before a frame is generated
     * @param dt time beetwen the last frame and the current frame
     */

    override function update(dt:Float) {
		
            currentScene.update(dt);
            
        
    }

    /**
     * code below is executed each time the window is resized ( don't work on js )
     */

    override function onResize() {
        currentScene.onResize();
    }
    
    



}
