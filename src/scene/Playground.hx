package scene;

import hxd.res.DefaultFont;
import h2d.Interactive;
import h2d.Bitmap;
import hxd.Res;
import h2d.Scene;
import h2d.Text;
import hxd.Window;
import h2d.Console;
import h2d.Tile;
import scene.gameUI.TextButton;

    /**
    * Class to create a screen display of the main menu ( displayed when you start the game ).
    * You can insert all UI element that you want to display. 
    * You can find preBuild UI element in scene.gameUI
    */

class Playground extends DynamicScene  {
    private var scene:Scene;
    private var background:h2d.Bitmap;

    /**
     * Create a new scene of the options menu
     * if you want to display it don't forget to change the current scene of the game with Game.instance.setScene( insert the new scene )
     */

    public function new() {
        super();
        

        this.width = Window.getInstance().width;
        this.height = Window.getInstance().height;

        var bg = Tile.fromColor(0xff0000);
        bg.scaleToSize(height* 2.491, height+height*0.05);
        bg.setCenterRatio();
        background = new Bitmap(bg);
        //background.smooth = true; // ajoute de l'anti aliasing
        background.y = height*0.55;
        background.x = width*0.5;
        background.tile.scaleToSize(height* 2.491, height+height*0.2);
        background.tile.setCenterRatio();
        background.y = height*0.5;
        background.x = width*0.5;
        background.rotation = -0.04;
        
        addChild(background);

    }

    /**
     * code below is executed each time the window is resized ( don't work on js )
     */


    override public function onResize() {
        width = Window.getInstance().width;
        height = Window.getInstance().height;
        background.tile.scaleToSize(width, height);
        background.tile.setCenterRatio();
        background.y = height*0.5;
        background.x = width*0.5;
        
        
    }

    /**
    * code below is executed each time before a frame is generated
    * @param dt time beetwen the last frame and the current frame
    */

    override public function update(dt:Float) {
        
        // ajoute ton cube rouge qui bouge :)
    }
    
    
     
}