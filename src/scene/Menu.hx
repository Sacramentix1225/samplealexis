package scene;

import hxd.res.DefaultFont;
import h2d.Interactive;
import h2d.Bitmap;
import hxd.Res;
import h2d.Scene;
import h2d.Text;
import hxd.Window;
import h2d.Console;
import h2d.Tile;
import scene.gameUI.TextButton;

    /**
    * Class to create a screen display of the main menu ( displayed when you start the game ).
    * You can insert all UI element that you want to display. 
    * You can find preBuild UI element in scene.gameUI
    */

class Menu extends DynamicScene  {
    private var scene:Scene;
    private var background:h2d.Bitmap;
    private var title:Text;
    private var playButton:TextButton;

    /**
     * Create a new scene of the options menu
     * if you want to display it don't forget to change the current scene of the game with Game.instance.setScene( insert the new scene )
     */

    public function new() {
        super();
        

        this.width = Window.getInstance().width;
        this.height = Window.getInstance().height;

        var bg = Tile.fromColor(0x0a0a0a);
        bg.scaleToSize(height* 2.491, height+height*0.05);
        bg.setCenterRatio();
        background = new Bitmap(bg);
        background.smooth = true;
        background.y = height*0.55;
        background.x = width*0.5;
        background.tile.scaleToSize(width, height);
        background.tile.setCenterRatio();
        background.y = height*0.5;
        background.x = width*0.5;
        background.rotation = -0.04;
        
        title = new h2d.Text(DefaultFont.get());
        title.textColor = 0xD4AF37;
        title.text ="Title";
        title.maxWidth = width;
        title.textAlign = Center;
        title.x = 0;
        title.y = 15; // Change this if you change the font for good alignement
        title.smooth = true;
        

        playButton = new TextButton(350,100, DefaultFont.get(), "Play");
        playButton.x = (this.width-350)*0.5;
        playButton.y = (this.height-100)*0.6;
        playButton.interactive.onClick = function( e : hxd.Event ) {
            Game.currentScene = new Playground();
            Game.instance.setScene(Game.currentScene);
        }
        
        addChild(background);
        addChild(title);
        this.addChild(playButton);
         
    }

    /**
     * code below is executed each time the window is resized ( don't work on js )
     */


    override public function onResize() {
        width = Window.getInstance().width;
        height = Window.getInstance().height;
        background.tile.scaleToSize(width, height);
        background.tile.setCenterRatio();
        background.y = height*0.5;
        background.x = width*0.5;
        playButton.x = (this.width-350)*0.5;
        playButton.y = (this.height-100)*0.6;
        title.maxWidth = width;
        
        
    }

    /**
    * code below is executed each time before a frame is generated
    * @param dt time beetwen the last frame and the current frame
    */

    override public function update(dt:Float) {
        
        
    }
    
    
     
}